# IGODemo

[![CI Status](https://img.shields.io/travis/omkar b/IGODemo.svg?style=flat)](https://travis-ci.org/omkar b/IGODemo)
[![Version](https://img.shields.io/cocoapods/v/IGODemo.svg?style=flat)](https://cocoapods.org/pods/IGODemo)
[![License](https://img.shields.io/cocoapods/l/IGODemo.svg?style=flat)](https://cocoapods.org/pods/IGODemo)
[![Platform](https://img.shields.io/cocoapods/p/IGODemo.svg?style=flat)](https://cocoapods.org/pods/IGODemo)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IGODemo is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'IGODemo'
```

## Author

omkar b, omkar@flappjacks.com

## License

IGODemo is available under the MIT license. See the LICENSE file for more info.
